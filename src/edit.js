import { __ } from '@wordpress/i18n';
import { RawHTML } from '@wordpress/element';
import { format, dateI18n } from '@wordpress/date';
import { useBlockProps, InspectorControls } from '@wordpress/block-editor';
import { PanelBody, ToggleControl, QueryControls } from '@wordpress/components';
import { useSelect } from '@wordpress/data';
import './editor.scss';

export default function Edit({ attributes, setAttributes }) {
	const { numberOfPosts, order, orderBy, categories, displayFeaturedImage } =
		attributes;

	//Create an array of category IDs or an empty array.
	const catIDs =
		categories && categories.length > 0
			? categories.map((cat) => cat.id)
			: [];
	// Fetch posts with useSelect hook.
	const posts = useSelect(
		(select) => {
			return select('core').getEntityRecords('postType', 'post', {
				per_page: numberOfPosts,
				_embed: true,
				order,
				orderby: orderBy,
				categories: catIDs,
			});
		},
		[numberOfPosts, order, orderBy, categories]
	);

	const allCats = useSelect((select) => {
		return select('core').getEntityRecords('taxonomy', 'category', {
			per_page: -1,
		});
	}, []);
	// Create an object for category suggestions
	const catSuggestions = {};
	if (allCats) {
		for (let i = 0; i < allCats.length; i++) {
			const cat = allCats[i];
			catSuggestions[cat.name] = cat;
		}
	}

	const onDisplayFeaturedImageChange = (value) => {
		setAttributes({ displayFeaturedImage: value });
	};
	const onNumberOfItemsChange = (value) => {
		setAttributes({ numberOfPosts: value });
	};

	const onCategoryChange = (values) => {
		const hasNoSuggestions = values.some(
			(value) => typeof value === 'string' && !catSuggestions[value]
		);
		if (hasNoSuggestions) return;

		const updatedCats = values.map((token) => {
			return typeof token === 'string' ? catSuggestions[token] : token;
		});

		setAttributes({ categories: updatedCats });
	};

	//authors
	const authorIDs = posts?.map((post) => post.author).join(',');
	const authors = useSelect(
		(select) => {
			return select('core').getEntityRecords('root', 'users', {
				per_page: -1,
				include: authorIDs,
			});
		},
		[authorIDs]
	);

	return (
		<>
			<InspectorControls>
				<PanelBody>
					<ToggleControl
						label={__('Display Featured Image', 'author')}
						checked={displayFeaturedImage}
						onChange={onDisplayFeaturedImageChange}
					/>
					<QueryControls
						numberOfItems={numberOfPosts}
						onNumberOfItemsChange={onNumberOfItemsChange}
						maxItems={10}
						minItems={1}
						orderBy={orderBy}
						onOrderByChange={(value) =>
							setAttributes({ orderBy: value })
						}
						order={order}
						onOrderChange={(value) =>
							setAttributes({ order: value })
						}
						categorySuggestions={catSuggestions}
						selectedCategories={categories}
						onCategoryChange={onCategoryChange}
					/>
				</PanelBody>
			</InspectorControls>
			<ul {...useBlockProps()}>
				{posts &&
					posts.map((post) => {
						const featuredImage =
							post._embedded &&
							post._embedded['wp:featuredmedia'] &&
							post._embedded['wp:featuredmedia'].length > 0 &&
							post._embedded['wp:featuredmedia'][0];
						const author = post._embedded?.author?.[0];
						//const authorTwitter = author?.swp_twitter;
						//const authorFacebook = author?.swp_facebook;
						return (
							<li key={post.id}>
								{displayFeaturedImage && featuredImage && (
									<img
										src={featuredImage.source_url}
										alt={featuredImage.alt_text}
									/>
								)}
								<h5>
									<a href={post.link}>
										{post.title.rendered ? (
											<RawHTML>
												{post.title.rendered}
											</RawHTML>
										) : (
											('(No title)', 'author')
										)}
									</a>
								</h5>
								{post.date_gmt && (
									<time dateTime={format('c', post.date_gmt)}>
										{dateI18n('F j, Y', post.date_gmt)}
									</time>
								)}
								{author && (
									<div className="post-author">
										{('By', 'Author Details:')}
										{/* Avatar:{' '} */}
										<div
											style={{
												display: 'flex',
												alignItems: 'center',
											}}
										>
											<img
												src={author.avatar_urls['48']}
												alt={author.name}
												style={{
													borderRadius: '50%',
													width: '60px',
													height: '60px',
												}}
											/>
											<div style={{ marginLeft: '16px' }}>
												<a href={author.link}>
													{author.name}
												</a>
											</div>
										</div>
										<p>
											<span
												style={{
													fontWeight: 600,
													padding: '4px',
												}}
											>
												Discription:
											</span>
											{author.description}
										</p>
										<div
											style={{
												padding: '5px',
												display: 'flex',

												alignItems: 'center',
											}}
										>
											<a
												style={{
													marginRight: '16px',
													paddingTop: '-16px',
												}}
												href={author.swp_twitter}
											>
												<span
													className="dashicons dashicons-twitter"
													style={{ fontSize: '24px' }}
												></span>
											</a>
											<a
												style={{
													padding: '4px',
												}}
												href={author.swp_facebook}
											>
												<span
													className="dashicons dashicons-facebook"
													style={{ fontSize: '24px' }}
												></span>
											</a>
										</div>
										<div
											style={{
												display: 'flex',
												flexDirection: 'column',
												alignItems: 'center',
											}}
										>
											<a
												href={post.link}
												target="_blank"
												rel="noreferrer"
											>
												<button
													style={{
														backgroundColor:
															'#a73232',
														color: 'white',
														border: 'none',
														borderRadius: '5px',
														padding: '8px 16px',
														marginTop: '16px',
														marginBottom: '16px',
														cursor: 'pointer',
														hover: 'blue',
													}}
												>
													View Post
												</button>
											</a>
										</div>
									</div>
								)}
								{authors &&
									authors.map((a) => (
										<div key={a.id}>
											<a href={a.link}>{a.name}</a>
											Bio:
											<a href={a.link}>{a.description}</a>
										</div>
									))}

								{/* {post.excerpt.rendered && (
									<RawHTML>{post.excerpt.rendered}</RawHTML>
								)} */}
							</li>
						);
					})}
			</ul>
		</>
	);
}
