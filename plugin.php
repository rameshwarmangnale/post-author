
<?php
/**
 * Plugin Name:       Author
 * Description:       Get post author details
 * Requires at least: 5.7
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            Ram
 * Text Domain:       author
 *
 * @package           blocks-course
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/writing-your-first-block-type/
 */

function blocks_course_render_author_block($attributes)
{
  $args = array(
    'posts_per_page' => $attributes['numberOfPosts'],
    'post_status' => 'publish'
  );
  $recent_posts = get_posts($args);

  $posts = '<ul ' . get_block_wrapper_attributes() . '>';
  foreach ($recent_posts as $post) {
    $posts .= '<li>';
    $title = get_the_title($post);
    $title = $title ? $title : __('(No title)', 'author');
    $permalink = get_permalink($post);
    $excerpt = get_the_excerpt($post);
    // Get author details
    $author = get_userdata($post->post_author);

    if ($attributes["displayFeaturedImage"] && has_post_thumbnail($post)) {
      $posts .= get_the_post_thumbnail($post, 'large');
    }
    // Get author avatar
    $author_avatar = get_avatar($author->ID, 60);

    $posts .= '<h5><a href="' . esc_url($permalink) . '">' . $title . '</a></h5>';
    $posts .= '<time datetime="' . esc_attr(get_the_date('c', $post)) . '">' . esc_html(get_the_date('', $post)) . '</time>';

    $posts .= '<div class="author-container" style="display: flex; align-items: center;">';
    $posts .= '<div class="author-avatar" style="border-radius: 50%; width: 60px; height: 60px; overflow: hidden; margin-right: 10px;">' . $author_avatar . '</div>';
    $posts .= '<div class="author-details"><a href="' . get_author_posts_url($post->post_author) . '">' . $author->display_name . '</a></div>'; // Display author details
    $posts .= '</div>';
    $author_id = $post->post_author;
    $author_bio = get_the_author_meta('description', $author_id);
    $posts .= '<p> <span style="font-weight:bold;"> Discription: 
		</span> <a href="' . get_author_posts_url($author_id) . '">' . $author_bio . '</a></p>';


    $twitter_username = get_the_author_meta('swp_twitter', $author_id);
    $facebook_url = get_the_author_meta('swp_fb_author', $author_id);
    $posts .= '<div class="author-social-icons">';

    if (!empty($facebook_url)) {
      $posts .= '<a style="padding: 4px;" href="' . esc_url($facebook_url) . '"><i class="dashicons dashicons-facebook"></i></a>';
    }


    if (!empty($twitter_username)) {
      $twitter_url = 'https://twitter.com/' . $twitter_username;
      $posts .= '<a style="padding: 4px;" href="' . esc_url($twitter_url) . '"><i class="dashicons dashicons-twitter"></i></a>';
    }

    // if (!empty($excerpt)) {
    // 	$posts .= '<p>' . $excerpt . '</p>';
    // }
    $posts .= '<div style="display: flex; justify-content: center;">';
    $posts .= '<a href="' . esc_url($permalink) . '" target="_blank" rel="noopener noreferrer"><button style="background-color: #a73232; color: white; border: none; border-radius: 5px; padding: 8px 16px; margin: 16px; cursor: pointer;">View Post</button></a>';
    $posts .= '</div>';
    $posts .= '</div>';
    $posts .= '</li>';
  }
  $posts .= '</ul>';

  return $posts;
}


function blocks_course_author_block_init()
{
  register_block_type_from_metadata(__DIR__, array(
    'render_callback' => 'blocks_course_render_author_block'
  ));
}

add_action('init', 'blocks_course_author_block_init');
