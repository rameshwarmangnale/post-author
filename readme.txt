=== Plugin Details ===
Plugin name: Author
Plugin description: Get post author details
Requires at least: 5.7
Requires PHP: 7.0
Version: 0.1.0
Author: Ram
Text Domain: author

=== Author WordPress Block ===
The Author block is a custom WordPress block that displays details about the authors of posts. This block is implemented as a plugin that can be installed on any WordPress website.

=== Installation ===
To install the Author block plugin:

Download the author directory to your local machine.
Upload the author directory to the wp-content/plugins/ directory on your WordPress installation.
Activate the Author block plugin from the WordPress plugins menu.
 
=== Block Details ===
The Author block displays the following information for each post author:

Post title: Clicking the title takes you to the post's permalink.
Author name: Clicking the name takes you to the author's archive page.
Author avatar: The author's profile picture.
Author bio: A short description of the author's biography.
Social icons: Links to the author's social media profiles (Twitter and Facebook).
Post date: The date when the post was published.
Post excerpt: A short summary of the post's content.

=== Usage ===
To use the Author block:

Edit a post in the WordPress editor.
Add a new block and search for "Author" or find it under the "Common Blocks" section.
Configure the block settings, including the number of posts to display and whether to show the featured image.
Publish or update the post to display the Author block.

=== Development ===
Requirements
WordPress 5.7 or later
PHP 7.0 or later

=== Code Structure ===
The plugin.php file contains the following functions:

blocks_course_render_author_block: The main function that renders the Author block's HTML code based on the given attributes.
blocks_course_author_block_init: The function that registers the Author block as a custom WordPress block.
